const express=require('express');
const config = require('../config')
const bcrypt=require('bcryptjs');
const flash=require('connect-flash');
const session=require('express-session');
const User=require("../models/User");
const passport=require('passport');



module.exports.register = [
    function(req, res) {
       
        const { name, email, password, password2} =req.body;
   let errors=[];
   if(!name || !email || !password || !password2){
        errors.push({ msg : "please fill in the fields"});
   }
   if(password != password2){
    errors.push({ msg : "passwords does not match"});
   }
   if(password.length < 6){
    errors.push({ msg : "password should be atleast 6 characters"});
   }
   if(errors.length > 0){
      res.render('register',{
          errors,
          name,
          email,
          password,
          password2
      });
   }else{
      User.findOne({ email : email})
      .then(user =>{
          if(user){
              errors.push({ msg: "email is already registered"});
            res.render('register',{
                errors,
                name,
                email,
                password,
                password2
            });   
          }else{
           const newUser= new User({
            name,
            email,
            password
            });

            bcrypt.genSalt(10, (err,salt)=> 
            bcrypt.hash(newUser.password,salt, (err,hash)=>{
               if(err) throw err;

               newUser.password=hash;
               newUser.save()
               .then(user => {
                   req.flash('success_msg', 'you are now registered and can login now');
                  // res.redirect("/login");
                  res.writeHead(301,{ 'Location': 'http://' + 'localhost:3000' + '/login'  });
                  return res.end();
               })
               .catch(err => console.log(err))
            }))
          }
      });
   }
    },
]

module.exports.login = [
    function(req, res, next){
        passport.authenticate('local',{
            successRedirect : "/display",
            failureRedirect: "/login",
            failureFlash:true
        })(req,res,next)
    },
]

 module.exports.logout = [
    function(req, res) {
        req.logout();
        req.flash("success_msg","You have successfully logged out");
        res.writeHead(301,{ 'Location': 'http://' + 'localhost:3000' + '/login'  });
        return res.end();
    },
]
