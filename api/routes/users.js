
const config = require('../config')
const { Router } = require('express')
const { ensureAuthenticated } = require('../config');

const router = Router();



const usersController = require('../controllers/usersController')


router.post('/register', usersController.register)
router.get('/register', usersController.register)
router.get('/logout',usersController.logout)
router.get('/display', ensureAuthenticated)
router.get('/pan', ensureAuthenticated)

router.post('/login', usersController.login)





module.exports = router;