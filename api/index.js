
const express=require('express');
const expressLayouts=require('express-ejs-layouts');
const db = require('./db')
const passport=require('passport');
const session=require('express-session');
const flash=require('connect-flash');


const app = express()
require('./config/passport')(passport);

require('./config/passport');


app.use(session({ 	
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
    
  }));

  app.use(passport.initialize());
  app.use(passport.session());

  app.use(flash());




app.use(express.json());
app.use(express.urlencoded({ extended: true }));

  app.use((req,res,next)=>{
      res.locals.success_msg= req.flash('success_msg');
      res.locals.error_msg= req.flash('error_msg');
      res.locals.error= req.flash('error');
      next();
    });



const users = require('./routes/users')



app.use(users)



module.exports = {
  path: '/',
  handler: app
}