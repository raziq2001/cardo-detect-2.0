module.exports = {
    ensureAuthenticated: function(req,res,next){
        if(req.isAuthenticated()){
         return next();
        }
        req.flash('error_msg',"Please log in to view this resource");
        res.writeHead(301,{ 'Location': 'http://' + 'localhost:3000' + '/login'  });
        return res.end();
    }
}